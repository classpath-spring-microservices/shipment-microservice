package com.classpath.shipment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Error> handleInvalidShipment(Exception exception){
		return new ResponseEntity<Error>(new Error(111, exception.getMessage()), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler (MethodArgumentNotValidException.class)
	public ResponseEntity<Error> handleInvalidData(Exception exception){
		return new ResponseEntity<Error>(new Error(222, exception.getMessage()), HttpStatus.BAD_REQUEST);
	}
}

@Setter
@Getter
@AllArgsConstructor
class Error {
	private int code;
	
	private String message;
	
}
