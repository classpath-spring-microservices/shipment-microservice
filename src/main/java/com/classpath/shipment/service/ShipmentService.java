package com.classpath.shipment.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.classpath.shipment.model.Shipment;

public interface ShipmentService {
	
	Shipment save(Shipment shipment);
	
	Page<Shipment>fetchAll(Pageable pageable);
	
	Shipment findById(long id);
	
	Shipment updateShipmentById(long shipmentId, Shipment shipment);

	void deleteById(long shipmentId);
}
