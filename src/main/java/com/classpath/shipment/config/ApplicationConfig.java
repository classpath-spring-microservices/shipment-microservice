package com.classpath.shipment.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnJava;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.system.JavaVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

	@Bean
	@ConditionalOnClass(name="com.classpath.shipment.controller.ShipmentController")
	public User javaxDataSource() { 
		return new User();
	}
	
	@Bean
	@ConditionalOnMissingBean(name="userBean")
	public User userBeanOnBeanName() { 
		return new User();
	}
	
}

class User {
	
}

