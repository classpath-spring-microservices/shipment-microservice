package com.classpath.shipment.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.classpath.shipment.model.Shipment;

@Repository
public interface ShipmentRepository extends JpaRepository<Shipment, Long>{
	
	Page<Shipment> findAll(Pageable pageble);
	
	List<Shipment> findByDeliveryDateBetween(LocalDate startDate, LocalDate endDate);
	
	List<Shipment> findByDeliveryDateBefore(LocalDate startDate);

	List<Shipment> findByDeliveryDateAfter(LocalDate endDate);
	
	@Query("select shipment from Shipment shipment where shipment.destination = ?1")
	List<Shipment> findByDestination(String destination);
}
