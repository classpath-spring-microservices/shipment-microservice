package com.classpath.shipment.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.classpath.shipment.model.Shipment;
import com.classpath.shipment.service.ShipmentService;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/v1/shipment")
@AllArgsConstructor
public class ShipmentController {

	private ShipmentService shipmentService;

	@GetMapping
	public ResponseEntity<Map<String, Object>> fetchShipments(@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="5") int size ){
		Pageable pageable = PageRequest.of(page, size);
		
		Map<String, Object> response = new HashMap<>();
		Page<Shipment> pageResponse = this.shipmentService.fetchAll(pageable);
		response.put("totalPages", pageResponse.getTotalPages());
	    response.put("totalElements", pageResponse.getTotalElements());
	    response.put("shipments", pageResponse.getContent());
		
	   return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Shipment shipProduct(@RequestBody @Valid Shipment shipment) {
		return this.shipmentService.save(shipment);
	}

	@GetMapping(value="/{id}", produces=MediaType.APPLICATION_XML_VALUE)
	public Shipment findById(@PathVariable long id) {
		return this.shipmentService.findById(id);
	}

	@PutMapping("/{id}")
	public Shipment updateShipment(@PathVariable long id, @RequestBody Shipment shipment) {
		return this.shipmentService.findById(id);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void deleteShipment(@PathVariable long id) {
		this.shipmentService.deleteById(id);
	}
}
